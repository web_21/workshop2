const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require("jsonwebtoken");

// Connect to local db
mongoose.connect('mongodb://localhost/dbstudents', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
let db = mongoose.connection;

// Check connection
db.once('open', function() {
    console.log('Connected to MongoDB');
});

// Check for db errors
db.on('error', function(err) {
    console.log(err);
});

const theSecretKey = '123';
const app = express();

// Basic auth
// const basicAuth = require('express-basic-auth')
// const {
//     base64decode
// } = require('nodejs-base64');
const cors = require("cors");

// check for cors
app.use(cors());

let Student = require('./models/student');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

// Set public folder
app.use(express.static(path.join(__dirname, 'public')));

// do not validate on authentication api
app.post("/", function(req, res, next) {
    if (req.body.username && req.body.password &&
        req.body.username === 'admin' && req.body.password === 'password') {
        const token = jwt.sign({
            userId: 123,
            name: 'Minor',
            permission: ['create', 'edit', 'delete']
        }, theSecretKey);

        res.status(201).json({
            token
        })
    } else {
        next();
    }
});

// JWT Authentication
app.use(function(req, res, next) {
    if (req.headers["authorization"]) {
        const authToken = req.headers['authorization'].split(' ')[1];
        try {
            jwt.verify(authToken, theSecretKey, (err, decodedToken) => {
                if (err || !decodedToken) {
                    res.status(401);
                    res.json({
                        error: "Unauthorized "
                    });
                }
                console.log('Welcome', decodedToken.name);
                next();
            });
        } catch (e) {
            next();
        }

    } else {
        res.status(401);
        res.send({
            error: "Unauthorized "
        });
    }
});


// custom basic authentication
// app.use(function(req, res, next) {
//     if (req.headers["authorization"]) {
//         const authBase64 = req.headers['authorization'].split(' ');
//         const userPass = base64decode(authBase64[1]);
//         const user = userPass.split(':')[0];
//         const password = userPass.split(':')[1];

//         //
//         if (user === 'admin' && password == '1234') {
//             next();
//             return;
//         }
//     }
//     res.status(401);
//     res.send({
//         error: "Unauthorized "
//     });
// });

// using basic auth
// app.use(basicAuth({
//     users: {
//         'admin': '1234',
//         'user1': 'supersecret1',
//         'user2': 'supersecret2',
//     }
// }));

// // handle the routes
// app.get("/api/tasks", getMethod);
// app.post("/api/tasks", postMethod);

// Get students from db
app.get('/', function(req, res) {
    Student.find({}, function(err, students) {
        if (err) {
            res.send(err);
        } else {
            // res.render('index', {
            //     title: 'Students',
            //     students: students
            // });
            res.json(students);
        }
    });
});

// Add students to the db
// app.get('/students/add', function(req, res) {
//     res.render('add_student', {
//         title: 'Add Student',
//     });
// });

// Add submit POST
app.post('/students/add', function(req, res) {
    let student = new Student();
    student.firstname = req.body.name;
    student.lastname = req.body.lastname;
    student.email = req.body.email;
    student.address = req.body.address;
    student.save(function(err) {
        if (err) {
            // console.log(err);
            // return;
            res.status(422);
            console.log('error while saving the task', err)
            res.json({
                error: 'There was an error saving the task'
            });
        }
        res.status(201);
        res.header({
            'location': `/`
        });
        res.json(task);
    });
});

// Edit student
app.get('/student/edit/:id', function(req, res) {
    Student.findById(req.params.id, function(err, student) {
        res.render('edit_student', {
            title: 'Edit student',
            student: student
        });
    });
});

// Update submit POST
app.post('/students/edit/:id', function(req, res) {
    let student = {};
    student.firstname = req.body.name;
    student.lastname = req.body.lastname;
    student.email = req.body.email;
    student.address = req.body.address;

    let query = { _id: req.params.id };

    Student.update(query, student, function(err) {
        if (err) {
            console.log(err);
            return;
        } else {
            res.redirect('/')
        }
    });
});

// Delete a student
app.delete('/student/:id', function(req, res) {
    let query = { _id: req.params.id };

    Student.remove(query, function(err) {
        if (err) {
            console.log(err);
        }
        res.send('Success');
    });
});

// handle 404
app.use(function(req, res, next) {
    res.status(404);
    res.send({
        error: "Not found"
    });
    return;
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));