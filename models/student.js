let mongoose = require('mongoose');

// Article Schema
let studentSchema = mongoose.Schema({
    firstname:{
        type: String,
        require: true
    },
    lastname:{
        type: String,
        require: true
    },
    email:{
        type: String,
        require: true
    },
    address:{
        type: String,
        require: true
    }
});

let Student = module.exports = mongoose.model('Student', studentSchema);
